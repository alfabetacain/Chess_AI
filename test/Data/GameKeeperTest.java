/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author christian
 */
public class GameKeeperTest {
    
    public GameKeeperTest() {
    }
    
    private static IGameKeeper keeper;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
        keeper = null;
    }
    
    @Before
    public void setUp() {
        keeper = new GameKeeper(null, null);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of startNewGame method, of class GameKeeper.
     */
    @Test
    public void testStartNewGame() {
        Board oldBoard = keeper.getBoard();
        Board newBoard = keeper.getBoard();
        Assert.assertTrue(oldBoard == newBoard);
    }

    /**
     * Test of getBoard method, of class GameKeeper.
     */
    @Test
    public void testGetBoard() {
        Board result = keeper.getBoard();
        Assert.assertFalse(result == null);
    }

    /**
     * Test of move method, of class GameKeeper.
     */
    @Test
    public void testMove() {
        //test move black pawn forward
        int x = 0;
        int y = 1;
        int destX = 0;
        int destY = 2;
        Move move = new Move(x, y, destX, destY);
        keeper.move(move);
        Piece piece = keeper.getBoard().getPiece(destX, destY);
        Assert.assertTrue(piece != null);
        Assert.assertTrue(piece.getColor() == PlayerColor.WHITE);
        Assert.assertTrue(piece.getType() == PieceType.PAWN);
    }

    /**
     * Test of isMoveValid method, of class GameKeeper.
     */
    @Test
    public void testIsMoveValid() {
    }
    
}
