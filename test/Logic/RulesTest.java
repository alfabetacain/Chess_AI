package Logic;

import Data.Board;
import Data.PlayerColor;
import Data.Piece;
import Data.PieceType;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author christian
 */
public class RulesTest {
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    /**
     * Tests that no line is not a straight line
     */
    @Test
    public void testNoLine() {
        Assert.assertFalse("No line should not be a straight line!", 
                Rules.isStraightLine(0, 0, 0, 0));
    }    
    
    /**
     * Short horizontal line (length 1) should be straight
     */
    @Test
    public void testShortHorizontalLine() {
        int x = 0;
        int destX = 1;
        int y = 0;
        int destY = 0;
        assertTrue("short horizontal line (length 1) should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Tests that a long horizontal line (length 7) is straight
     */
    @Test
    public void testLongHorizontalLine() {
        int x = 0;
        int destX = 7;
        int y = 0;
        int destY = 0;
        assertTrue("Long horizontal line (length 7) should be straight",
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Tests that a negative move (reverse move) horizontally
     * is still considered a straight line
     */
    @Test
    public void testNegativeHorizontalLine() {
        int x = 7;
        int destX = 0;
        int y = 0;
        int destY = 0;
        assertTrue("Negative move (reverse move) horizontally "
                + "should still be a straight line", 
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Short vertical line (length 1) should be straight
     */
    @Test
    public void testShortVerticalLine() {
        int x = 0;
        int destX = 0;
        int y = 0;
        int destY = 1;
        assertTrue("short vertical line (length 1) should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Tests that a long vertical line (length 7) is straight
     */
    @Test
    public void testLongVerticalLine() {
        int x = 0;
        int destX = 0;
        int y = 0;
        int destY = 7;
        assertTrue("Long vertical line (length 7) should be straight",
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Tests that a reverse vertical move still is considered a straight line
     */
    @Test
    public void testNegativeVerticalLine() {
        int x = 0;
        int destX = 0;
        int y = 7;
        int destY = 0;
        assertTrue("Negative vertical move (reverse move)"
                + " should still be considered a straight line", 
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Tests that a straight line with length exceeding
     * the maximum for a chess board is still considered a straight 
     * line
     */
    @Test
    public void testStraightLineWithExcessiveValues() {
        int x = 100;
        int destX = 1000;
        int y = 200;
        int destY = 200;
        assertTrue("A straight line is straight, not matter if "
                + "it is longer than a chess board is wide", 
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Tests that a diagonal line is not straight
     */
    @Test
    public void testDiagonalLineIsNotStraight() {
        int x = 0;
        int destX = 1;
        int y = 0;
        int destY = 1;
        assertFalse("A diagonal line is not straight", 
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Test that a line which is not straight nor truly diagonal 
     * is not considered straight. Line: deltaX = 2, deltaY = 1
     */
    @Test
    public void testNotDiagonalNorStraightLineIsNotStraight() {
        int x = 0;
        int destX = 2;
        int y = 0;
        int destY = 1;
        assertFalse("Line which is not truly diagonal nor straight"
                + " should not be straight", 
                Rules.isStraightLine(x, y, destX, destY));
    }
    
    /**
     * Tests that a straight clear line should be clear
     */
    @Test
    public void testClearVerticalStraightLineIsClear() {
        Board board = new Board();
        board.killPiece(0, 1); //kill piece in front of black rook
        int x = 0;
        int destX = 0;
        int y = 0;
        int destY = 4;
        assertTrue("Clear vertical straight line should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
        assertTrue("Clear vertical straight line should be clear", 
                Rules.isStraightLineClear(board, x, y, destX, destY));
    }
    
    @Test
    public void testNotClearVerticalStraightLineIsNotClear() {
        Board board = new Board();
        //do not kill piece in front of black rook
        //line is not clear
        int x = 0;
        int destX = 0;
        int y = 0;
        int destY = 4;
        assertTrue("Not clear vertical straight line should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
        assertFalse("Not clear vertical straight line should not be clear", 
                Rules.isStraightLineClear(board, x, y, destX, destY));
    }
    
    @Test
    public void testClearHorizontalStraightLineIsClear() {
        Board board = new Board();
        board.killPiece(1, 0); //kill knight next to black rook
        board.killPiece(2, 0); //kill bishop next to black rook
        int x = 0;
        int destX = 2;
        int y = 0;
        int destY = 0;
        assertTrue("Clear horizontal straight line should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
        assertTrue("Clear horizontal straight line should be clear", 
                Rules.isStraightLineClear(board, x, y, destX, destY));
    }
    
    @Test
    public void testNotClearHorizontalStraightLineIsNotClear() {
        Board board = new Board();
        //do not kill piece in front of black rook
        //line is not clear
        int x = 0;
        int destX = 2;
        int y = 0;
        int destY = 0;
        assertTrue("Not clear horizontal straight line should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
        assertFalse("Not clear horizontal straight line should not be clear", 
                Rules.isStraightLineClear(board, x, y, destX, destY));
    }
    
    @Test
    public void testSameColorIsSameColor() {
        Piece one = new Piece(PieceType.PAWN, PlayerColor.BLACK);
        Piece two = new Piece(PieceType.PAWN, PlayerColor.BLACK);
        assertTrue("Two black pawns should be the same color",
                Rules.isSameColor(one, two));
    }
    
    @Test
    public void testNotSameColorIsNotSameColor() {
        Piece one = new Piece(PieceType.PAWN, PlayerColor.BLACK);
        Piece two = new Piece(PieceType.PAWN, PlayerColor.WHITE);
        assertFalse("Two black pawns should be the same color",
                Rules.isSameColor(one, two));
    }
    
    @Test
    public void testTwoNullIsNotSameColor() {
        assertFalse("Two nulls do not have the same color", 
                Rules.isSameColor(null, null));
    }
    
    @Test
    public void testOneNullIsNotSameColor() {
        Piece piece = new Piece(PieceType.PAWN, PlayerColor.BLACK);
        assertFalse("One null and one not null does not have the same color",
                Rules.isSameColor(null, piece));
    }
    
    @Test
    public void testDiagonalLineIsDiagonal() {
        int x = 0;
        int y = 0;
        int destX = 2;
        int destY = 2;
        assertTrue("Diagonal line should be diagonal", 
                Rules.isDiagonalLine(x, y, destX, destY));
    }
    
    @Test
    public void testStraightHorizontalLineIsNotDiagonal() {
        int x = 0;
        int y = 0;
        int destX = 5;
        int destY = 0;
        assertTrue("Straight line should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
        assertFalse("Straight line should not be diagonal", 
                Rules.isDiagonalLine(x, y, destX, destY));
    }
    
    @Test
    public void testStraightVerticalLineIsNotDiagonal() {
        int x = 0;
        int y = 0;
        int destX = 0;
        int destY = 5;
        assertTrue("Straight line should be straight", 
                Rules.isStraightLine(x, y, destX, destY));
        assertFalse("Straight line should not be diagonal", 
                Rules.isDiagonalLine(x, y, destX, destY));
    }
    
    @Test
    public void testNoLineShouldNotBeDiagonal() {
        assertFalse("Line with length 0 should not be diagonal", 
                Rules.isDiagonalLine(0, 0, 0, 0));
    }
    
    @Test
    public void testNotCompletelyDiagonalLineIsNotDiagonal() {
        int x = 0;
        int y = 0;
        int destX = 2;
        int destY = 3;
        assertFalse("Not completely diagonal line should not be diagonal", 
                Rules.isDiagonalLine(x, y, destX, destY));
    }
    
    @Test
    public void testClearDiagonalLineIsClear() {
        Board board = new Board();
        //kill black pawn to the right of the black bishop
        board.killPiece(3, 1); 
        int x = 2;
        int y = 0;
        int destX = 4;
        int destY = 2;
        assertTrue("Clear diagonal line should be clear", 
                Rules.isDiagonalLineClear(board, x, y, destX, destY));
    }
    
    @Test
    public void testNotClearDiagonalLineIsNotClear() {
        Board board = new Board();
        //do not kill black pawn to the right of the black bishop
        int x = 2;
        int y = 0;
        int destX = 4;
        int destY = 2;
        assertFalse("Clear diagonal line should be clear", 
                Rules.isDiagonalLineClear(board, x, y, destX, destY));
    }
    
    @Test
    public void testKnightLineIsKnightLine() {
        int x = 0;
        int y = 0;
        int destX = 2;
        int destY = 1;
        assertTrue("Knight line should be knight line", 
                Rules.isKnightLine(x, y, destX, destY));
    }
    
    @Test
    public void testStraightHorizontalLineIsNotKnightLine() {
        int x = 0, y = 0, destX = 2, destY = 0;
        assertFalse("Straight horizontal line should not be knight line", 
                Rules.isKnightLine(x, y, destX, destY));
    }
    
    @Test
    public void testStraightVerticalLineIsNotKnightLine() {
        int x = 0, y = 0, destX = 0, destY = 2;
        assertFalse("Straight vertical line should not be knight line",
                Rules.isKnightLine(x, y, destX, destY));
    }
    
    @Test
    public void testDiagonalLineShouldNotBeKnightLine() {
        int x = 0, y = 0, destX = 2, destY = 2;
        assertFalse("Diagonal line should not be knight line", 
                Rules.isKnightLine(x, y, destX, destY));
    }
    
    @Test
    public void testNoLineShouldNotBeKnightLine() {
        assertFalse("Line of length = 0 should not be knight line", 
                Rules.isKnightLine(0, 0, 0, 0));
    }
    
    @Test
    public void testClearKnightLineIsClear() {
        Board board = new Board();
        int x = 1, y = 0, destX = 2, destY = 2;
        assertTrue("Clear knight line should be clear", 
                Rules.isKnightLineClear(board, x, y, destX, destY));
    }
    
    @Test
    public void testNotClearKnightLineIsNotClear() {
        Board board = new Board();
        int x = 1, y = 0, destX = 3, destY = 1;
        assertFalse("Not clear knight line should not be clear", 
                Rules.isKnightLineClear(board, x, y, destX, destY));
    }
}
