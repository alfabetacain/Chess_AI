# AI and GUI for chess
---
The purpose of this repository is:
<ol>
<li>To provide a GUI for presenting a view of a chess board and to provide replayability</li>
<li>To provide a way to check for the validity of a chess move</li>
<li>To provide an AI for playing chess (basic)</li>
<li>To provide a way to communicate with a telnet based chess server (link to api will be provided)</li>
</ol>

---

This project is licensed under the [GNU GPL](http://www.gnu.org/copyleft/gpl.html)
