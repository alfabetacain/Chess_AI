package Controller;

import Board.GameView;
import Data.GameKeeper;
import Data.IGameKeeper;
import Data.Move;
import Logic.Rules;

/**
 *
 * @author christian
 */
public class Controller {
    private final IGameKeeper keeper;
    
    public Controller(IGameKeeper keeper) {
        if (keeper == null)
            throw new IllegalArgumentException("Keeper cannot be null");
        this.keeper = keeper;
    }

    public static void main(String[] args) {
        IGameKeeper keeper = new GameKeeper();
        Controller controller = new Controller(keeper);
        keeper.startNewGame();
        GameView game = new GameView(keeper);
        game.pack();
        game.setVisible(true);
        try {
            Thread.sleep(1000);
            System.out.println("Moving");
            if (controller.move(1, 1, 2, 2))
                System.out.println("Piece moved");
            else
                System.out.println("Piece did not move");
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }
    
    public boolean move(int x, int y, int destX, int destY) {
        Move move = new Move(x, y, destX, destY);
        if (Rules.isMoveValid(keeper.getBoard(), x, y, destX, destY)) {
            keeper.move(move);
            return true;
        }
        return false;
    }
}
