package Data;

/**
 *
 * @author christian
 */
public abstract class PieceFactory {

    public static Piece createPiece(PieceType type, PlayerColor color) {
        switch (type) {
            case PAWN:
                return new Piece(PieceType.PAWN, color);
            case ROOK:
                return new Piece(PieceType.ROOK, color);
            case KNIGHT:
                return new Piece(PieceType.KNIGHT, color);
            case BISHOP:
                return new Piece(PieceType.BISHOP, color);
            case QUEEN:
                return new Piece(PieceType.QUEEN, color);
            case KING:
                return new Piece(PieceType.KING, color);
            default:
                return new Piece(PieceType.PAWN, color);
        }
    }
}
