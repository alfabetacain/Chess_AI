package Data;

import Player.InGamePlayer;

/**
 *
 * @author christian
 */
public interface IGameKeeper extends Observable {
    public Board getBoard();
    public boolean move(Move move);
    public void load(Board board);
    public void loadFromHistory(int i);
    public void start();
    public void resume();
    public PlayerColor getColor(InGamePlayer player);
}
