package Data;

import Logic.Rules;
import Player.InGamePlayer;
import Player.Player;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author christian
 */
public class GameKeeper implements IGameKeeper, Observable {

    private Board board;
    private final List<Observer> observers;
    private final InGamePlayer whitePlayer, blackPlayer;
    private GameStatus gameStatus;
    private Player winner;
    private final List<PlayerMove> history;

    public GameKeeper(InGamePlayer whitePlayer, InGamePlayer blackPlayer) {
        observers = new ArrayList<>();
        history = new ArrayList<>();
        board = new Board();
        this.whitePlayer = whitePlayer;
        whitePlayer.setColor(PlayerColor.WHITE);
        this.blackPlayer = blackPlayer;
        blackPlayer.setColor(PlayerColor.BLACK);
        winner = null;
        gameStatus = GameStatus.NOT_BEGUN;
    }

    @Override
    public Board getBoard() {
        return board;
    }

    /**
     * Moves a piece. A piece must move some distance, the method will 
     * return false if distance == 0
     * @param move
     * @return true if move is valid, false otherwise
     */
    @Override
    public boolean move(Move move) {
        if (move.getFromX() == move.getToX() && 
                move.getFromY() == move.getToY())
            return false;
        if (board.getPiece(move.getToX(), move.getToY()) != null) {
            board.killPiece(move.getToX(), move.getToY());
        }
        board.move(move.getFromX(), move.getFromY(),
                move.getToX(), move.getToY());
        changeOccurred();
        calculateGameStatus();
        return true;
    }
    
    private void calculateGameStatus() {
        //determine if checkmate
        if (Rules.isAnyPlayerCheckMate(board))
            gameStatus = GameStatus.CHECKMATE;
        //determine if draw
    }
    
    @Override
    public void load(Board board) {
        this.board = board;
        changeOccurred();
    }
    
    @Override
    public void loadFromHistory(int i) {
        changeOccurred();
    }

    @Override
    public void addObserver(Observer obs) {
        if (obs != null)
            observers.add(obs);
    }
    
    private void changeOccurred() {
        for (Observer obs : observers)
            obs.notifyOfChange();
    }
    
    @Override
    public void start() {
        gameStatus = GameStatus.IN_PROGRESS;
        while (!gameStatus.hasEnded()) {
            
            //white
            conductPlayerMove(whitePlayer);
            
            calculateGameStatus();
            if (gameStatus.hasEnded())
                break;
            
            //black
            conductPlayerMove(blackPlayer);
            calculateGameStatus();
        }
        
        //determine who won
        if (winner == null) {
            switch (gameStatus) {
                case RESIGNED:
                    winner = history.get(history.size()-1).
                            getPlayerColor() == PlayerColor.BLACK ?
                            whitePlayer : blackPlayer;
                    break;
                case CHECKMATE:
                    winner = history.get(history.size()-1).
                            getPlayerColor() == PlayerColor.BLACK ?
                            whitePlayer : blackPlayer;
                    break;
                case DRAW:
                    winner = null;
                    break;
            }
        }
    }
    
    private void conductPlayerMove(InGamePlayer player) {
        boolean hasMoved = false;
        Move move, oldMove;
        oldMove = null;
        while (!hasMoved) {
            move = player.getMove(oldMove);
                if (move == null) {
                    gameStatus = GameStatus.RESIGNED;
                    winner = player == whitePlayer ? blackPlayer : whitePlayer;
                }
                else if (move(move)) {
                    hasMoved = true;
                    history.add(new PlayerMove(player, move));
                }
                else
                    oldMove = move;
        }
    }
    
    @Override
    public void resume() {
        changeOccurred();
        start();
    }

    @Override
    public PlayerColor getColor(InGamePlayer player) {
        return whitePlayer == player ? PlayerColor.WHITE : PlayerColor.BLACK;
    }
}
