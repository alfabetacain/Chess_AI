package Data;

/**
 *
 * @author christian
 */
public class Piece {
    private final PlayerColor color;
    private final PieceType type;
    private int x,y;
    
    public Piece(PieceType type, PlayerColor color) {
        this.type = type;
        this.color = color;
    }

    public PlayerColor getColor() {
        return color;
    }
    
    public PieceType getType() {
        return type;
    }
    
    public String getDescription() {
        return type.getDescription();
    }    
    
    public void changePosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public Piece copy() {
        Piece piece = new Piece(type, color);
        piece.changePosition(x, y);
        return piece;
    }
}
