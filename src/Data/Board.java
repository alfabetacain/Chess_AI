package Data;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author christian
 */
public class Board {
    private final Piece[][] chessPieces;
    private final List<Piece> blackPieces, whitePieces;
    
    public Board() {        
        chessPieces = new Piece[8][8];
        
        blackPieces = new ArrayList<>();
        whitePieces = new ArrayList<>();
        
        //white
        fillBackRow(chessPieces[0], PlayerColor.WHITE);
        addRowToColor(true, chessPieces[0]);
        fillPeasants(chessPieces[1], PlayerColor.WHITE);
        addRowToColor(true, chessPieces[1]);
        //black
        fillBackRow(chessPieces[7], PlayerColor.BLACK);
        addRowToColor(false, chessPieces[7]);
        fillPeasants(chessPieces[6], PlayerColor.BLACK);
        addRowToColor(false, chessPieces[6]);
    }
    
    private void fillBackRow(Piece[] line, PlayerColor color) {
        line[0] = PieceFactory.createPiece(PieceType.ROOK, color);
        line[1] = PieceFactory.createPiece(PieceType.KNIGHT, color);
        line[2] = PieceFactory.createPiece(PieceType.BISHOP, color);
        line[3] = PieceFactory.createPiece(PieceType.QUEEN, color);
        line[4] = PieceFactory.createPiece(PieceType.KING, color);
        line[5] = PieceFactory.createPiece(PieceType.BISHOP, color);
        line[6] = PieceFactory.createPiece(PieceType.KNIGHT, color);
        line[7] = PieceFactory.createPiece(PieceType.ROOK, color);
        
    }
    
    private void fillPeasants(Piece[] line, PlayerColor color) {
        for (int i = 0; i < line.length; i++)
            line[i] = PieceFactory.createPiece(PieceType.PAWN, color);
    }
    
    private void addRowToColor(boolean isBlack, Piece[] row) {
        if (isBlack)
            blackPieces.addAll(Arrays.asList(row));
        else
            whitePieces.addAll(Arrays.asList(row));
    }
    
    public Piece[][] getBoard() {
        return chessPieces;
    }
    
    public void move(int x, int y, int destX, int destY) {
        Piece piece = chessPieces[y][x];
        chessPieces[y][x] = null;
        chessPieces[destY][destX] = piece;
        piece.changePosition(destX, destY);
    }
    
    public Piece getPiece(int x, int y) {
        return chessPieces[y][x];
    }
    
    public Piece killPiece(int x, int y) {
        Piece piece = chessPieces[y][x];
        if (piece == null)
            return piece;
        if (piece.getColor() == PlayerColor.BLACK)
            blackPieces.remove(piece);
        else
            whitePieces.remove(piece);
        chessPieces[y][x] = null;
        return piece;
    }
    
    public Iterable<Piece> getLivePieces(boolean isBlack) {
        return isBlack ? blackPieces : whitePieces;
    }
    
    public Board copy() {
        Board copy = new Board();
        Piece[][] arrayCopy = copy.getBoard();
        for (int y = 0; y < chessPieces.length; y++)
            for (int x = 0; x < chessPieces[y].length; x++) {
                if (chessPieces[y][x] == null)
                    arrayCopy[y][x] = null;
                else {
                    Piece pieceCopy = chessPieces[y][x].copy();
                    if (pieceCopy.getColor() == PlayerColor.BLACK)
                        copy.blackPieces.add(pieceCopy);
                    else
                        copy.whitePieces.add(pieceCopy);
                    arrayCopy[y][x] = chessPieces[y][x].copy();
                }
            }
        return copy;
    }
}
