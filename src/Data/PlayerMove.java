package Data;

import Player.InGamePlayer;

/**
 *
 * @author christian
 */
public class PlayerMove {
    private final Move move;
    private final InGamePlayer player;
    
    public PlayerMove(InGamePlayer player, Move move) {
        this.player = player;
        this.move = move;
    }
    
    public int getFromX() {
        return move.getFromX();
    }

    public int getFromY() {
        return move.getFromY();
    }

    public int getToX() {
        return move.getToX();
    }

    public int getToY() {
        return move.getToY();
    }
    
    public PlayerColor getPlayerColor() {
        return player.getPlayerColor();
    }
    
}
