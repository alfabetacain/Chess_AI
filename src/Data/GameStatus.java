/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author christian
 */
public enum GameStatus {
    NOT_BEGUN(false), IN_PROGRESS(false), DRAW(true), RESIGNED(true),
    CHECKMATE(true);
    
    private final boolean hasEnded;
    
    private GameStatus(boolean hasEnded) {
        this.hasEnded = hasEnded;
    }
    
    public boolean hasEnded() {
        return hasEnded;
    }
}
