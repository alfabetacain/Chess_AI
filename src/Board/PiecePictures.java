package Board;

import Data.PieceType;
import Data.PlayerColor;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

/**
 *
 * @author christian
 */
public class PiecePictures {

    private static PiecePictures me;
    private final Map<PieceType, Image[]> pictures;
    private static final String PICTURE_FOLDER = 
            "./Pictures/";
    
    private PiecePictures() {
        pictures = new HashMap<>();
        for (PieceType type : PieceType.values()) {
            Image[] images = new Image[2];
            String path = PICTURE_FOLDER + "white_" + 
                    type.toString().toLowerCase() + ".png";
            try {
                Image img = ImageIO.read(new File(path));
                images[0] = img;
            } 
            catch (IOException e) {
                System.out.println(e);
            }
            path = PICTURE_FOLDER + "black_" + 
                    type.toString().toLowerCase() + ".png";
            try {
                Image img = ImageIO.read(new File(path));
                images[1] = img;
            } 
            catch (IOException e) {
                System.out.println(e);
            }
            pictures.put(type, images);
        }
    }
    
    public static Image getPicture(PieceType type, PlayerColor color) {
        if (me == null)
            me = new PiecePictures();
        Image[] images = me.pictures.get(type);
        if (color == PlayerColor.BLACK)
            return images[1];
        return images[0];
    }
    
    
    public static PiecePictures getPiecePictures() {
        if (me == null) {
            me = new PiecePictures();
        }
        return me;
    }
}
