package Board;

import Data.IGameKeeper;
import Data.Observer;
import Data.Piece;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;

/**
 *
 * @author christian
 */
public class BoardView extends JPanel implements Observer {
    private final IGameKeeper gameKeeper;
    
    public BoardView(IGameKeeper keeper) {
        super();
        gameKeeper = keeper;
        keeper.addObserver(this);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Dimension cell = getCellDimension();
        int width = cell.width;
        int height = cell.height;
        paintBackground(g);
        Piece[][] board = gameKeeper.getBoard().getBoard();
        for (int y = 0; y < board.length; y++) {
            for (int x = 0; x < board[y].length; x++) {
                Piece piece = board[y][x];
                if (piece != null) {
                    Image img = PiecePictures.getPicture(piece.getType(), piece.getColor());
                    g.drawImage(img, x*width, y*height, width, height, null);
                }
            }
        }
    }
    
    private void paintBackground(Graphics g) {
        Dimension cell = getCellDimension();
        int width = cell.width;
        int height = cell.height;
        for (int y = 0; y < 8; y++)
            for (int x = 0; x < 8; x++) {
                g.drawRect(x*width, y*height, width, height);
                if ((y+x) % 2 != 0)
                    g.fillRect(x*width, y*height, width, height);
            }
    }
    
    private Dimension getCellDimension() {
        Dimension size = getSize();
        Dimension cell = new Dimension(size.width/8, size.height/8);
        return cell;
    }

    @Override
    public void notifyOfChange() {
        System.out.println("Repainting");
        repaint();
    }
}
