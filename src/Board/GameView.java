package Board;

import Controller.Controller;
import Data.GameKeeper;
import Data.IGameKeeper;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author christian
 */
public class GameView extends JFrame {
    private final IGameKeeper keeper;
    private final BoardView board;
    
    public GameView(IGameKeeper keeper) {
        super();
        this.keeper = keeper;
        setLayout(new BorderLayout());
        board = new BoardView(keeper);
        add(board, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Game View");
        setSize(500, 500);
        setPreferredSize(new Dimension(500, 500));
    }
}
