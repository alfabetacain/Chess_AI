package Board;

import Data.IGameKeeper;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataListener;

/**
 *
 * @author christian
 */
public class HistoryView extends JPanel {
    private final IGameKeeper keeper;
    private JList history;
    
    public HistoryView(IGameKeeper keeper) {
        this.keeper = keeper;
        ListModel list = new ListModel() {

            @Override
            public int getSize() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public Object getElementAt(int index) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addListDataListener(ListDataListener l) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void removeListDataListener(ListDataListener l) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        history = new JList();
        history.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        history.setLayoutOrientation(JList.VERTICAL);
    }
}
