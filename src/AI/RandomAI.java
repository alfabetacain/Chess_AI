package AI;

import Data.IGameKeeper;
import Data.Move;
import Data.PlayerColor;
import Player.InGamePlayer;

/**
 *
 * @author christian
 */
public class RandomAI implements InGamePlayer {

    public IGameKeeper game;
    
    public RandomAI() {
        game = null;
    }
    
    @Override
    public Move getMove(Move oldMove) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PlayerColor getPlayerColor() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setGame(IGameKeeper game) {
        this.game = game;
    }
}
