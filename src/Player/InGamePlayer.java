/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Player;

import Data.IGameKeeper;
import Data.Move;
import Data.PlayerColor;

/**
 *
 * @author christian
 */
public interface InGamePlayer extends Player {
    
    public void setColor(PlayerColor color);
    public PlayerColor getPlayerColor();
    /**
     * Method for getting a move from a player. 
     * oldMove is typically the last move, but in essence
     * is just a move, that is invalid and should not be attempted 
     * again.
     * If oldMove == null, then there are no restrictions on the moves
     * that may be returned, besides the game rules that is. 
     * It is simply a courtesy to let the player know the it is 
     * meaningless to attempt that move again.
     * Returning null is equal to forfeiting the game. 
     * @param oldMove an invalid move that should not be attempted
     * @return the next move from the player. Returning null is equal to
     * forfeiting the game.
     */
    public Move getMove(Move oldMove);
    
    public void setGame(IGameKeeper game);
}
