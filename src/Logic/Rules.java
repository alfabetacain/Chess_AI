package Logic;

import Data.Board;
import Data.PlayerColor;
import Data.Move;
import Data.Piece;
import Data.PieceType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author christian
 */
public final class Rules {
    
    private Rules() {
        throw new IllegalStateException(
                "Rules class should never be instantiated");
    }
    
    public static boolean isAnyPlayerCheckMate(Board board) {
        return isPlayerCheckMate(board, true) 
                || isPlayerCheckMate(board, false);
    }
    
    public static boolean isPlayerCheckMate(Board board, 
            boolean useBlackPlayer) {
        Piece king = null;
        Iterator<Piece> iterator = 
                board.getLivePieces(useBlackPlayer).iterator();
        while (iterator.hasNext()) {
            Piece current = iterator.next();
            if (useBlackPlayer) {
                if (current.getColor() == PlayerColor.BLACK
                        && current.getType() == PieceType.KING) {
                    king = current;
                    break;
                }
            }
            else {
                if (current.getColor() == PlayerColor.WHITE 
                        && current.getType() == PieceType.KING) {
                    king = current;
                    break;
                }
            }
        }
        if (king == null)
            return false;
        List<Move> moves = new ArrayList<>();
        int x,y;
        x = king.getX();
        y = king.getY();
        moves.add(new Move(x,y,x-1,y));
        moves.add(new Move(x,y,x+1,y));
        moves.add(new Move(x,y,x,y-1));
        moves.add(new Move(x,y,x,y+1));
        moves.add(new Move(x,y,x-1,y-1));
        moves.add(new Move(x,y,x+1,y+1));
        moves.add(new Move(x,y,x-1,y+1));
        moves.add(new Move(x,y,x+1,y-1));
        
        for (Move move : moves) {
            if (isMoveValid(board, move))
                return false;
        }
        return true;
    }
    
    public static boolean isMoveValid(Board board, Move move) {
        return isMoveValid(board, move.getFromX(), move.getFromY(),
                move.getToX(), move.getToY());
    }
    
    public static boolean isMoveValid(Board board, 
            int x, int y, int destX, int destY) {
        if (board == null)
            throw new IllegalArgumentException("Board cannot be null");
        if (Math.abs(destX-x) == 0 && Math.abs(destY-y) == 0)
            return false; //must make a move
        Piece piece = board.getPiece(x, y);
        if (piece == null)
            return false; //must move a piece
        MoveType type = getMoveType(x, y, destX, destY);
        if (isValidMoveForType(type, piece, x, y, destX, destY)) 
            return isLineClear(type, board, x, y, destX, destY);
        return false;
    }
    
    public static MoveType getMoveType(int x, int y, int destX, int destY) {
        if (Math.abs(destX-x) == 0 && Math.abs(destY-y) == 0)
            return MoveType.NO_LINE;
        if (isStraightLine(x, y, destX, destY))
            return MoveType.STRAIGHT_LINE;
        if (isDiagonalLine(x, y, destX, destY))
            return MoveType.DIAGONAL_LINE;
        if (isKnightLine(x, y, destX, destY))
            return MoveType.KNIGHT_LINE;
        return MoveType.INVALID_LINE;
    }
    /**
     * Tests that a line is straight given two points
     * A line can be straight even if the line goes from positive
     * to negative
     * @param x
     * @param y
     * @param destX
     * @param destY
     * @return whether or not a line is straight
     */
    public static boolean isStraightLine(int x, int y, int destX, int destY) {
        return x != destX ^ y != destY;
    }
    public static boolean isDiagonalLine(int x, int y, int destX, int destY) {
        int xdiff = Math.abs(destX-x);
        int ydiff = Math.abs(destY-y);
        if (xdiff == 0 || ydiff == 0)
            return false;
        return xdiff == ydiff;
    }
    public static boolean isKnightLine(int x, int y, int destX, int destY) {
        int xdiff = Math.abs(destX-x);
        int ydiff = Math.abs(destY-y);
        if (xdiff == 0 || ydiff == 0)
            return false;
        return xdiff == ydiff+1 ^ ydiff == xdiff+1;
    }
    
    public static boolean isLineClear(MoveType type, 
            Board board, int x, int y, int destX, int destY) {
        if (board == null)
            throw new IllegalArgumentException("Board cannot be null");
        switch (type) {
            case STRAIGHT_LINE:
                return isStraightLineClear(board, x, y, destX, destY);
            case DIAGONAL_LINE:
                return isDiagonalLineClear(board, x, y, destX, destY);
            case KNIGHT_LINE:
                return isKnightLineClear(board, x, y, destX, destY);
            case NO_LINE:
                return false;
            case INVALID_LINE:
                return false;
            default:
                return false;
        }
    }
    public static boolean isStraightLineClear(Board board,
            int x, int y, int destX, int destY) {
        if (!isStraightLine(x, y, destX, destY))
            return false;
        //horizontal line
        if (x != destX) {
            int start = x < destX ? x : destX;
            int end = x < destX ? destX : x;
            //road
            for (int i = start+1; i < end; i++) {
                if (board.getPiece(i, y) != null)
                    return false;
            }
            //final stop
            if (isSameColor(board, x, y, destX, destY))
                return false;
        }
        else { //vertical line
            int start = y < destY ? y : destY;
            int end = y < destY ? destY : y;
            //road
            for (int i = start+1; i < end; i++) {
                if (board.getPiece(x, i) != null)
                    return false;
            }
            //final stop
            if (isSameColor(board, x, y, destX, destY))
                return false;
        }
        return true;
    }
    public static boolean isSameColor(Board board, int x, int y, int x2, int y2) {
        if (board == null)
            throw new IllegalArgumentException("Board cannot be null");
        return isSameColor(board.getPiece(x, y), board.getPiece(x2, y2));
    }
    public static boolean isSameColor(Piece one, Piece two) {
        if (one == null || two == null) 
            return false;
        return one.getColor() == two.getColor();
    }
    /**
     * If line is not diagonal, returns false
     * @param board
     * @param x
     * @param y
     * @param destX
     * @param destY
     * @return 
     */
    public static boolean isDiagonalLineClear(Board board, int x, int y, int destX, int destY) {
        if (!isDiagonalLine(x, y, destX, destY))
            return false;
        if (board == null)
            throw new IllegalArgumentException("Board cannot be null");
        int tempX = x;
        int tempY = y;
        Piece origin = board.getPiece(x, y);
        if (origin == null)
            throw new IllegalArgumentException("Origin piece to move"
                    + " cannot be null");
        while (tempX != destX) {
            tempX += tempX < destX ? 1 : -1;
            tempY += tempY < destY ? 1 : -1;
            Piece piece = board.getPiece(tempX, tempY);
            if (tempX != destX && piece != null)
                return false;
            if (tempX == destX && piece != null && isSameColor(origin, piece))
                return false;
        }
        return true;
    }
    public static boolean isKnightLineClear(Board board, 
            int x, int y, int destX, int destY) {
        if (!isKnightLine(x, y, destX, destY))
            return false;
        if (board == null)
            throw new IllegalArgumentException("Board cannot be null");
        Piece origin = board.getPiece(x, y);
        Piece target = board.getPiece(destX, destY);
        return target == null || 
                origin != null && origin.getColor() != target.getColor();
    }
    
    public static boolean isValidMoveForType(MoveType type, Piece piece,
            int x, int y, int destX, int destY) {
        switch (piece.getType()) {
            case PAWN:
                if (type != MoveType.STRAIGHT_LINE)
                    return false;
                int number = getNumberOfFields(type, x, y, destX, destY);
                if (number != 1)
                    return false;
                if (piece.getColor() == PlayerColor.BLACK)
                    return destY > y;
                else
                    return y > destY;
            case KNIGHT:
                return type == MoveType.KNIGHT_LINE;
            case BISHOP:
                return type == MoveType.DIAGONAL_LINE;
            case ROOK:
                return type == MoveType.STRAIGHT_LINE;
            case QUEEN:
                return type == MoveType.DIAGONAL_LINE ||
                        type == MoveType.STRAIGHT_LINE;
            case KING:
                return getNumberOfFields(type, x, y, destX, destY) == 1;
            default:
                return false;
        }
    }
    
    public static int getNumberOfFields(MoveType type, 
            int x, int y, int destX, int destY) {
        switch (type) {
            case KNIGHT_LINE:
                return 1;
            case STRAIGHT_LINE:
                return Math.max(Math.abs(destX-x), Math.abs(destY-y));
            case DIAGONAL_LINE:
                //distance moved along one axis is 
                //the same as on the other axis
                return Math.abs(destY-y); 
            case NO_LINE: 
                return 0;
            case INVALID_LINE:
                return -1;
            default:
                return -1;
        }
    }
}
